<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Regex(pattern="/^[[:alnum:]]([-_.]?[[:alnum:]])+_?@[[:alnum:]]([-.]?[[:alnum:]])+\.[a-z]{2,6}$/",
     * match=true, message="Entrez une adresse email valide")
     */
    private string $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\Regex(pattern="/\d/", match=false, message="Votre prénom de peut pas contenir de chiffres")
     * @Assert\Regex(pattern="/^[a-zA-Zàâéèêîïôûù ]+$/", match=true, message="Les caractères spéciaux sont interdits")
     */
    private string $first_name;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\Regex(pattern="/\d/", match=false, message="Votre prénom de peut pas contenir de chiffres")
     * @Assert\Regex(pattern="/^[a-zA-Zàâéèêîïôûù ]+$/", match=true, message="Les caractères spéciaux sont interdits")
     */
    private string $last_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $picture;

    /**
     * @ORM\Column(type="datetime")
     */
    private \Datetime $dob;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\Regex(pattern="/^(?:(?:\+|00)33{0,3}(?:\(0\){0,3})?|0)[1-9](?:(?:\d{2}){4}|\d{2}(?:\d{3}){2})$/", 
     * match=true, message="Veuillez entrer un numéro valide sans aucun espace ni caractères")
     */
    private string $user_phone;

    /**
     * @ORM\Column(type="string", length=4)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9 ,.'()-]+$/", match=true, message="Veuillez entrez un numéro valide")
     */
    private string $address_number;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $address_type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[a-zA-Z 0-9,.'ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ-]+$/", 
     * match=true, message="Veuillez entrez un nom valide")
     */
    private string $address_name;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9 ,.'()-]+$/", match=true, message="Veuillez entrez un code postal valide")
     */
    private string $address_zipcode;

    /**
     * @ORM\Column(type="string", length=120)
     * @Assert\Regex(pattern="/^[a-zA-Z ,.'()ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ-]+$/", 
     * match=true, message="Veuillez entrez un nom de ville valide")
     */
    private string $address_city;

    public function __toString()
    {
        return $this->first_name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getPicture(): string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getDob(): \DateTime
    {
        return $this->dob;
    }

    public function setDob(\DateTime $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    public function getUserPhone(): string
    {
        return $this->user_phone;
    }

    public function setUserPhone(string $user_phone): self
    {
        $this->user_phone = $user_phone;

        return $this;
    }

    public function getAddressNumber(): string
    {
        return $this->address_number;
    }

    public function setAddressNumber(string $address_number): self
    {
        $this->address_number = $address_number;

        return $this;
    }

    public function getAddressType(): string
    {
        return $this->address_type;
    }

    public function setAddressType(string $address_type): self
    {
        $this->address_type = $address_type;

        return $this;
    }

    public function getAddressName(): string
    {
        return $this->address_name;
    }

    public function setAddressName(string $address_name): self
    {
        $this->address_name = $address_name;

        return $this;
    }

    public function getAddressZipcode(): string
    {
        return $this->address_zipcode;
    }

    public function setAddressZipcode(string $address_zipcode): self
    {
        $this->address_zipcode = $address_zipcode;

        return $this;
    }

    public function getAddressCity(): string
    {
        return $this->address_city;
    }

    public function setAddressCity(string $address_city): self
    {
        $this->address_city = $address_city;

        return $this;
    }
}
