<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'attr' => ['class' => 'form-select', 'id' => 'inputCategory'],
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Tarif',
                'divisor' => 100,
                'attr' => ['class' => 'form-control', 'id' => 'inputPrice'],
            ])
            ->add('picture', FileType::class, [
                'data_class' => null,
                'label' => 'Insérer une image',
                /*on précice au formulaire de ne pas ranger lui même 
                la donnée, car on ne veut stocker que le nom du fichier*/
                'mapped' => false,
                'required' => false,
                /*le champ n'étant pas mappé à l'entité on doit définir les contraintes ici*/
                'constraints' =>
                [
                /*la contrainte de type File permet de définir des contraines associées aux fichiers*/
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' =>
                        [
                            'image/png',
                            'image/jpeg',
                            'image/jpg',
                            'image/jfif',
                        ], 'mimeTypesMessage' => 'Seul les fichiers png, jpg, jfif sont acceptés',
                    ])
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => ['class' => 'tinymce form-control', 'id' => 'inputDescription'],
            ])
            ->add('deletePicture', CheckboxType::class, [
                'label' => 'Télécharger image par défault',
                'required' => false,
                'mapped' => false
            ]);
            // On pourrait ajouter le bouton submit via le formulaire :
            // ->add('submit', SubmitType::class, [
            //     'label' => 'valider',
            // ])
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
