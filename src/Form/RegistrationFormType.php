<?php

namespace App\Form;


use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class, [
                'label' => 'Prénom',
                'required' => true,
                'constraints' => new NotBlank([
                    'message' => 'Veuillez indiquer le prénom'
                ]),
            ])
            ->add('last_name', TextType::class, [
                'label' => 'NOM',
                'constraints' => new NotBlank(),
            ])
            ->add('dob', BirthdayType::class, [
                'placeholder' => [
                    'year' => 'année', 'day' => 'Jour', 'month' => 'Mois',
                ],
                'label' => 'date de naissance'
            ])
            ->add('user_phone', TelType::class, [
                'label' => 'Téléphone',
                'label_format' => '--.address.%name%',
                'attr' => [],
            ])
            ->add('address_number',TextType::class, [
                'label' => 'numéro de rue',
            ])
            ->add('address_type', ChoiceType::class, [
                'label' => 'type (rue, av, impasse,...)',
                'choices' => [
                    'Choisissez parmi la liste' => 'placeholder',
                    'avenue' => 'av',
                    'rue' => 'rue',
                    'place' => 'pl',
                    'impasse' => 'imp',
                    'boulevard' => 'bd',
                    'Carrefour' => 'Car',
                    'chemin' => 'che',
                    'faubourg' => 'fg',
                    'passage' => 'pas',
                    'promenade' => 'pro',
                    'route' => 'rte',
                    'plaine' => 'pln',
                    'montée' => 'mte',
                    'esplanade' => 'esp',
                    'domaine' => 'dom',
                    'square' => 'sq',
                ]
            ])
            ->add('address_name', TextType::class, [
                'label' => 'nom de rue',
            ])
            ->add(
                'address_zipcode',
                TextType::class,
                array(
                    'label' => 'Code Postal',
                    'attr' => array('maxlength' => 10),
                    'required' => false,
                    'empty_data' => null
                )
            )

            ->add('address_city', TextType::class, [
                'label' => 'ville'
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email',
                'attr' => [
                    'autofocus' => false,
                    'class' => 'span8',
                    'placeholder' => 'example@example.com'
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'J\'ai lu et accepté les conditions générales',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions.',
                    ]),
                ],
            ])
            ->add('picture', FileType::class, [
                'label' => 'Ma photo',
                'mapped' => true,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpg',
                            'image/jpeg',
                            'image/jfif',
                        ],
                        'mimeTypesMessage' => 'Veuillez télécharger un fichier valide : png, jpg, jfif',
                    ])
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent correspondre',
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Répétez le mot de passe'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
