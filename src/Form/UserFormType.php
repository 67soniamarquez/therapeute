<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;



class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class, [
                'label' => 'Prénom',
                'constraints' => new NotBlank(),
            ])
            ->add('last_name', TextType::class, [
                'label' => 'NOM',
                'constraints' => new NotBlank(),
            ])
            ->add('dob', BirthdayType::class, [
                'placeholder' => [
                    'year' => 'année', 'day' => 'Jour', 'month' => 'Mois',
                ],
                'label' => 'date de naissance'
            ])
            ->add('user_phone', TelType::class, [
                'label' => 'Téléphone',
                'label_format' => '--.address.%name%',
                'attr' => [],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email',
                'attr' => [
                    'autofocus' => false,
                    'class' => 'span8',
                    'placeholder' => 'example@example.com'
                ]
            ])
            ->add('address_number', TextType::class, [
                'label' => 'numéro de rue'
            ])
            ->add('address_type', ChoiceType::class, [
                'label' => 'type (rue, av, impasse,...)',
                'required' =>true,
                'choices' => [
                    'Choisissez parmi la liste' => '-',
                    'avenue' => 'av',
                    'rue' => 'rue',
                    'place' => 'pl',
                    'impasse' => 'imp',
                    'boulevard' => 'bd',
                    'Carrefour' => 'Car',
                    'chemin' => 'che',
                    'faubourg' => 'fg',
                    'passage' => 'pas',
                    'promenade' => 'pro',
                    'route' => 'rte',
                    'plaine' => 'pln',
                    'montée' => 'mte',
                    'esplanade' => 'esp',
                    'domaine' => 'dom',
                    'square' => 'sq',
                ]
            ])
            ->add('address_name', TextType::class, [
                'label' => 'nom de rue',
            ])
            ->add(
                'address_zipcode',
                TextType::class,
                array(
                    'label' => 'Code Postal',
                    'attr' => array('maxlength' => 10),
                    'required' => false,
                    'empty_data' => null
                )
            )
            ->add('address_city', TextType::class, [
                'label' => 'ville'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
