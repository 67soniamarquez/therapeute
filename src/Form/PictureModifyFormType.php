<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class PictureModifyFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('pictureFile', FileType::class, [
            'label' => 'Photo de profil',
            'mapped' => false,
            'required' => false,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/png',
                        'image/jpeg',
                        'image/jfif'
                    ],
                    'mimeTypesMessage' => 'Seul les fichiers png, jfif et jpeg sont acceptés',
                    'maxSize' => '2048k'
                ])
            ],
        ])
            ->add('deletePicture', CheckboxType::class, [
                'label' => 'image par default',
                'required' => false,
                'mapped' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Modifier l\'image'
            ]);
    }
}
