<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user')]
class UserController extends AbstractController
{
    #[Route('/', name: 'user')]
    public function index(UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();

        if ($user->getRoles()[0] == 'ROLE_ADMIN') {

            $userRepository = $this->getDoctrine()->getRepository(User::class);
            $users = $userRepository->findAll();

            return $this->render('user/index.html.twig', [
                'users' => $users,
            ]);
        } else {
            $this->addFlash('danger', 'Accès refusé, vous n\'êtes pas l\'administrateur !');
            return $this->redirectToRoute('index');
        }
    }

    #[Route('/view/{id}', name: 'user_view')]
    public function view(UserRepository $userRepository, $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();
        $userRepository = $this->getDoctrine()->getRepository(User::class);

        if ($user->getRoles()[0] == 'ROLE_ADMIN') {
            $userView = $userRepository->find($id);
            if (!$userView) {
                $this->addFlash('danger', 'utilisateur inexistant avec l\'id : ' . $id);
                return $this->redirectToRoute('user');
            }
        } else {
            $userView = $userRepository->find($user->getId());
        }

        return $this->render(
            'user/view.html.twig',
            [
                'user' => $userView
            ]
        );
    }

    #[Route('/update/{id}', name: 'user_update', methods: ['GET', 'POST'])]
    public function update(User $user, Request $request, $id): Response
    {
    
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if($this->isCsrfTokenValid('update' . $user->getId(), $request->request->get('token'))) {
            $userConnect = $this->getUser();
            $user = $this->getDoctrine()->getRepository(User::class)->find($id);
    
            if ($userConnect->getRoles()[0] == 'ROLE_ADMIN') {
                if ($user) {
    
                    $form = $this->createForm(UserFormType::class, $user);
                    $form->handleRequest($request);
    
                    if ($form->isSubmitted() && $form->isValid()) {
                        $phone = $form->get('user_phone')->getData();
                        $phone = wordwrap($phone, 2, ' ', true);
                        $user->setUserPhone($phone);

                        $nom = $user->getLastName();
                        $prenom = $user->getFirstName();
                        $ville = $user->getAddressCity();
                        $user->setLastName(strtoupper($nom))
                            ->setFirstName(strtoupper($prenom))
                            ->setAddressCity(strtoupper($ville));

                        try {
                            $this->getDoctrine()->getManager()->flush();
                        } catch (Exception $e) {
                            throw new HttpException(500, 'Erreur lors de l\'enregistrement de la modification');
                        }
                        return $this->redirectToRoute('user_view', ['id' => $user->getId()]);
                    }
    
                    return $this->render(
                        'user/formUpdate.html.twig',
                        [
                            "user" => $user,
                            "updateForm" => $form->createView()
                        ]
                    );
                } else {
                    throw $this->createNotFoundException('utilisateur inexistant avec l\'id' . $id);
                }
            } else {
                $this->addFlash('danger', 'pas admin');
                return $this->redirectToRoute('index');
            }
            return $this->redirectToRoute('user'); 
        }
        return $this->redirectToRoute('user_view', ['id' => $user->getId()]);
    }

    #[Route('/delete/{id}', name: 'user_delete', methods: ['GET', 'POST'])]
    public function delete(User $user, Request $request, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($user) {
            if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('token'))) {

                try {
                    $em->remove($user);
                    $em->flush();
                } catch (Exception $e) {
                    $this->addFlash('danger', 'Suppression impossible');
                    return $this->redirectToRoute('user');
                }
            }
        }
        return $this->redirectToRoute('user');
    }
}
