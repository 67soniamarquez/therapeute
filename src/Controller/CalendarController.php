<?php

namespace App\Controller;

use App\Entity\Calendar;
use App\Form\CalendarType;
use App\Repository\CalendarRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/calendar')]
class CalendarController extends AbstractController
{
    #[Route('/', name: 'calendar_index', methods: ['GET'])]
    public function index(CalendarRepository $calendar): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('calendar/index.html.twig', [
            'calendars' => $calendar->findAll()
        ]);
    }

    #[Route('/new', name: 'calendar_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $calendar = new Calendar();
        $form = $this->createForm(CalendarType::class, $calendar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($calendar) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($calendar);
                try {
                    $entityManager->flush();

                    return $this->redirectToRoute('calendar_index');
                } catch (Exception $e) {
                    throw new HttpException(500, 'Une erreur est survenue lors de l\enregistrement !');
                }
            }
        }
        return $this->render('calendar/new.html.twig', [
            'calendar' => $calendar,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'calendar_show', methods: ['GET'])]
    public function show(Calendar $calendar): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('calendar/show.html.twig', [
            'calendar' => $calendar,
        ]);
    }

    #[Route('/{id}/edit', name: 'calendar_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Calendar $calendar): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(CalendarType::class, $calendar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($calendar) {
                try {
                    $this->getDoctrine()->getManager()->flush();
                } catch (Exception $e) {
                    throw new HttpException(500, 'erreur l\'ors de l\'enregistrement');
                }
                return $this->redirectToRoute('calendar_index');
            }
        }
        return $this->render('calendar/edit.html.twig', [
            'calendar' => $calendar,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/delete/{id}', name: 'calendar_delete', methods: ['POST'])]
    public function delete(Request $request, Calendar $calendar): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if ($calendar) {
            if ($this->isCsrfTokenValid('delete' . $calendar->getId(), $request->request->get('_token'))) {

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($calendar);

                try {
                    $entityManager->flush();
                    } catch (Exception $e) {
                        $this->addFlash('danger', 'erreur l\'ors de l\'enregistrement');
                        return $this->redirectToRoute('calendar_index');
                    }
            }
            return $this->redirectToRoute('calendar_index');
        } else {
            return throw new NotFoundHttpException ('Calendar inexistant');
        }
    }
}