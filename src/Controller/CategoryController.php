<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryFormType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/category')]
class CategoryController extends AbstractController
{
    #[Route('/', name: 'category')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->findAll();

        return $this->render('category/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    #[Route('/create', name: 'category_create')]
    public function create(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $category = new Category();

        $form = $this->createFormBuilder($category)
            ->add('name')
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'ajouter',
                    'attr' => array(
                        'class' => 'btn-coin btn-custom btn btn-category-create'
                    )
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($category) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($category);

                try {
                    $entityManager->flush();
                } catch (Exception $e) {
                    throw new HttpException(500, 'erreur l\'ors de l\'enregistrement');
                }

                return $this->redirectToRoute("category_view", ["id" => $category->getId()]);
            }
        }
        return $this->render(
            'category/create.html.twig',
            [
                "category" => $form->createView()
            ]
        );
    }

    #[Route('/view/{id}', name: 'category_view')]
    public function view($id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);

        $category = $categoryRepository->find($id);

        if ($category) {
            return $this->render(
                'category/view.html.twig',
                [
                    "category" => $category
                ]
            );
        } else {
            throw $this->createNotFoundException(
                'L\'id : ' . $id . ' est inexistant'
            );
        }
    }

    #[Route("/update/{id}", name: "category_update", methods: ['GET', 'POST'])]
    public function update(Category $category, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
       
        //  if ($this->isCsrfTokenValid('update' . $category->getId(), $request->request->get('token'))) {
        $user = $this->getUser();
        
        if ($user->getRoles()[0] == 'ROLE_ADMIN') {
            if (!$category) {
                return throw $this->createNotFoundException(
                    'L\'id : ' . $category->getId() . ' est inexistant'
                );
            }
            $form = $this->createFormBuilder($category)
                ->add('name')
                ->add('submit', SubmitType::class)
                ->getForm();
            $form->handleRequest($request);
            
            if ($form->isSubmitted() && $form->isValid()) {
                   // dd($form);
                        try {
                            $this->getDoctrine()->getManager()->flush();
                        } catch (Exception $e) {
                            throw new HttpException(500, 'Erreur lors de l\'enregistrement de la modification');
                        }
                        return $this->redirectToRoute('category_view', ['id' => $category->getId()]);
                    }
                    
                    return $this->render(
                        'category/update.html.twig',
                        [
                            'category' => $category,
                            'updateForm' => $form->createView()
                        ]
                    );
                } else {
                    $this->addFlash('danger', 'Accès refusé vous n\'êtes pas autorisé à cet accès!');
                    return $this->redirectToRoute('index');
                }
        
            return $this->redirectToRoute('category_view', ['id' => $category->getId()]);
        //    }else {
        //        $this->addFlash('danger', 'Accès refusé !');
        //        return $this->redirectToRoute('index');
        //    }
          return $this->redirectToRoute('category_view', ['id' => $category->getId()]);
    } 
    #[Route("/delete/{id}", name: "category_delete")]
    //la fonction retournera soit une response OU une NotFoundHttpException
    public function delete(Request $request, Category $category): Response

    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if ($category) {
            if ($this->isCsrfTokenValid('delete' . $category->getId(), $request->request->get('token'))) {

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($category);
                    try {
                        $entityManager->flush();
                    } catch (Exception $e) {
                        $this->addFlash('danger', 'Suppression impossible car la categorie est affectée à un produit');
                        return $this->redirectToRoute('category');
                    }
            }
            return $this->redirectToRoute('category');
        } else {
            return throw new NotFoundHttpException('Categorie inexistante');
        }
    }
}
