<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\PasswordModifyFormType;
use App\Form\PictureModifyFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Dompdf\Dompdf;
use Dompdf\Options;

#[Route('/profile')]
class ProfileController extends AbstractController
{
    #[Route('/', name: 'profile_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        return $this->render('profile/index.html.twig');
    }

    #[Route('/view/{email}', name: 'profile_view')]
    public function view(string $email, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(["email" => $email]);

        if (!$user) {
            $this->createNotFoundException('profil inexistant avec l\'email : ' . $email);
        }

        return $this->render('profile/view.html.twig', ["user" => $user]);
    }

    #[Route('/password-modify', name: 'profile_password_modify')]
    public function modifyPassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = $this->getUser();
        $this->denyAccessUnlessGranted('ROLE_USER');

        $form = $this->createForm(PasswordModifyFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //on remplace l'ancien mot de passe par le nouveau
            $user->setPassword(
                $encoder->encodePassword(
                    $user,
                    $form->get('newPassword')->getData()
                )
            );

            try {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            } catch (Exception $e) {
                $this->addFlash('danger', 'Erreur lors de l\'enregistrement');
            }
            return $this->redirectToRoute('profile_index');
        }

        return $this->render(
            'profile/modifyPassword.html.twig',
            [
                "passwordModifyForm" => $form->createView()
            ]
        );
    }

    #[Route('/picture-modify', name: 'profile_picture_modify')]
    public function modifyPicture(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();

        $form = $this->createForm(PictureModifyFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pictureFile = $form->get('pictureFile')->getData();
            $deletePicture = $form->get('deletePicture')->getData();

            if ($pictureFile && !$deletePicture) {
                $fileName = md5(uniqid(rand())) . "." . $pictureFile->guessExtension();
                $fileDestination = $this->getParameter('user_profile_pictures_dir');
                try {
                    $pictureFile->move($fileDestination, $fileName);
                } catch (FileException $e) {

                    throw new HttpException(500, 'Une erreur est survenue lors du téléchargement');
                }

                if ($user->getPicture() != 'defaultUser.jpg') {
                    $this->removeProfilePicture($user->getPicture());
                }
                $user->setPicture($fileName);
            }

            //si l'utilisateur décide de supprimer son image
            if ($deletePicture) {
                //on retire l'image
                $this->removeProfilePicture($user->getPicture());
                //et on donne l'image par défaut à l'utilisateur
                $user->setPicture('defaultUser.jpg');
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            try {
                $entityManager->flush();
            } catch (Exception $e) {
                $this->addFlash('danger', 'erreur lors de l\'enregistrement');
            }

            return $this->redirectToRoute('profile_index');
        }

        return $this->render('profile/modifyPicture.html.twig', ['modifyPictureForm' => $form->createView()]);
    }

    private function removeProfilePicture(string $path)
    {
        $fs = new Filesystem();
        try {
            $fs->remove($this->getParameter('user_profile_pictures_dir') . '/' . $path);
        } catch (IOException $e) {
            throw new HttpException(500, 'An error occured during file upload');
        }
    }

    #[Route('/data', name: 'profile_data')]
    public function data(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();
        return $this->render('profile/data.html.twig', ['user' => $user]);
    }

    #[Route('/data/download', name: 'profile_data_download')]
    public function dataDownload(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        //on définit les options du pdf
        $pdfOptions = new Options();

        //On définit la police par défaut
        $pdfOptions->set('defaultFont', 'Arial');
        //Lors du téléchargement, pour résoudre certains problèmes liés au SSL
        $pdfOptions->setIsRemoteEnabled(true);

        //On instancie Dompdf
        $dompdf = new Dompdf($pdfOptions);

        //servira lors de la mise en place du SSL
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verif_peer' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $dompdf->setHttpContext($context);

        //on génère le html
        $html = $this->renderView('profile/download.html.twig');

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        //On génère un nom de fichier
        $fichier = 'user-data' . $this->getUser()->getId() . '.pdf';

        //On envoie le PDF au navigateur
        $dompdf->stream($fichier, [
            'Attachment' => true
        ]);

        return new Response();
    }
}
