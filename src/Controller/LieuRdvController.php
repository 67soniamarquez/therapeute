<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LieuRdvController extends AbstractController
{
    #[Route('/lieu/rdv', name: 'lieu_rdv')]
    public function index(): Response
    {
        return $this->render('lieu_rdv/index.html.twig', [
            'lieuRdv' => 'Choix du lieu de la scéance',
        ]);
    }
}
