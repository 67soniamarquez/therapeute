<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MakeDateController extends AbstractController
{
    #[Route('/make/date', name: 'make_date')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('make_date/index.html.twig', [
            'controller_name' => 'MakeDateController',
        ]);
    }
}
