<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, 
    SluggerInterface $slugger): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $pictureFile = $form->get('picture')->getData();
            if ($pictureFile) {
                $originalFilename = pathinfo(
                    $pictureFile->getClientOriginalName(),
                    PATHINFO_FILENAME
                );
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . $pictureFile->guessExtension();

                try {
                    $pictureFile->move(
                        $this->getParameter('picture_user'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new HttpException(500, 'Une erreur s\'est produite durant le téléchargement du fichier');
                }
                $user->setPicture($newFilename);
            } else {
                $newFilename = 'defaultUser.jpg';
                $user->setPicture($newFilename);
            }

            //-gestion des enregistrement en bdd 
            $phone = $form->get('user_phone')->getData();
            $phone = wordwrap($phone, 2, ' ', true);
                $user->setUserPhone($phone);

            $nom = $user->getLastName();
            $prenom = $user->getFirstName();
            $ville = $user->getAddressCity();
            $user->setLastName(strtoupper($nom))
                ->setFirstName(strtoupper($prenom))
                ->setAddressCity(strtoupper($ville));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            try {
                $entityManager->flush();
            }catch (Exception $e) {
                $this->addFlash('danger', 'Le formualaire n\'est pas valide');
            }

            return $this->redirectToRoute('user');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
