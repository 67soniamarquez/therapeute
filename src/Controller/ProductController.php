<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductFormType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/product')]
class ProductController extends AbstractController
{
    #[Route('/', name: 'product', methods: ['GET'])]
    public function index(): Response
    {
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepository->findAll();

        return $this->render(
            'product/index.html.twig',
            [
                'products' => $products,
            ]
        );
    }

    #[Route('/create', name: 'product_create', methods: ['GET', 'POST'])]
    public function create(Request $request,  SluggerInterface $slugger): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $product = new Product();
        $form = $this->createForm(ProductFormType::class, $product);
        $form->handleRequest($request);
        $user = $this->getUser();

        // gestion de l'upload d'image
        $pictureFile = $form->get('picture')->getData();

        if ($user->getRoles()[0] == 'ROLE_ADMIN') {
            if ($form->isSubmitted() && $form->isValid()) {

                //  si on a reçu un fichier
                if ($pictureFile) {
                    $originalFilename = pathinfo($pictureFile->getClientOriginalName(),
                     PATHINFO_FILENAME);

                    //cela est nécessaire pour inclure en toute sécurité le nom du fichier 
                    //dans le cadre de l'URL
                    $safeFilename = $slugger->slug($originalFilename);

                    //génération du nom de fichier aléatoire + extension devinée à partir du type MIME
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $pictureFile->guessExtension();

                    try {
                        $pictureFile->move(
                            //on récupère le dossier des images tel que définit dans 
                            //le paramètre d'application dans config/services.yaml
                            $this->getParameter('picture_product'),
                            $newFilename
                        );
                    } catch (FileException $e) {
                        throw new HttpException(500, 'Une erreur s\'est produite durant le téléchargement du fichier');
                    }

                    //on enregistre le nom de l'image dans notre entité Product
                    $product->setPicture($newFilename);
                } else {
                    //sinon, on enregistre une image par défault
                    $newFilename = 'default.jpg';
                    $product->setPicture($newFilename);
                }
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($product);

                try {
                    $entityManager->flush();
                }catch (Exception $e) {
                    $this->addFlash('danger', 'erreur lors de l\'enregistrement !');
                    return $this->redirectToRoute('index');
                }

                return $this->redirectToRoute("product");
            }
        } else {
            $this->addFlash('danger', 'Accès refusé, vous n\'êtes pas l\'administrateur !');
            return $this->redirectToRoute('index');
        }
        return $this->render(
            'product/create.html.twig',
            [
                "createForm" => $form->createView()
            ]
        );
    }

    #[Route('/view/{id}', name: 'product_view', methods: ['GET'])]
    public function view($id): Response
    {
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $product = $productRepository->find($id);
        if (!$product) {
            throw $this->createNotFoundException(
                'Il n\'existe pas de produit avec cet id: ' . $id
            );
        }

        return $this->render(
            'product/view.html.twig',
            [
                "product" => $product
            ]
        );
    }

    #[Route('/update/{id}', name: 'product_update', methods: ['GET', 'POST'])]
    public function update(Request $request, int $id, SluggerInterface $slugger, Product $product): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->isCsrfTokenValid('update' . $product->getId(), $request->request->get('token'))) {
            $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
            $user = $this->getUser();
            
            if ($user->getRoles()[0] == 'ROLE_ADMIN') {
                if (!$product) {
                    throw $this->createNotFoundException(
                        'Il n\y pas de produit avec cet id:' . $id
                    );
                }
                $form = $this->createForm(ProductFormType::class, $product);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $pictureFile = $form->get('picture')->getData();
                    $deletePicture = $form->get('deletePicture')->getData();
                    //si on a reçu un fichier et qu'on ne souhaite pas revenir à l'image par défaut

                    if ($pictureFile && !$deletePicture) {

                        $originalFilename = pathinfo($pictureFile->getClientOriginalName(), PATHINFO_FILENAME);

                        $safeFilename = $slugger->slug($originalFilename);
                        $newFilename = $safeFilename . '-' . uniqid() . '.' . $pictureFile->guessExtension();

                        try {
                            $pictureFile->move(
                                $this->getParameter('picture_product'),
                                $newFilename
                            );
                        } catch (FileException $e) {
                            throw new HttpException(500, 'une erreur s\'est produit lors du téléchargement du fichier');
                        }
                        //si l'image n'était pas l'image par défaut
                        if ($product->getPicture() !== 'default.jpg') {

                            //on supprime l'ancienne image du serveur
                            $this->removeProductPicture($product->getPicture());
                        }
                        //on enregistre le nom de l'image dans notre entité Product
                        $product->setPicture($newFilename);
                    }

                    //si l'utilisateur décide de supprimer son image

                    if ($deletePicture || !$pictureFile) {
                        //on retire l'image
                        $this->removeProductPicture($product->getPicture());
                        //et on donne l'image par défaut à l'utilisateur
                        $product->setPicture('default.jpg');
                    }


                    $em = $this->getDoctrine()->getManager();

                    try {
                        $em->persist($product);
                        $em->flush();
                    } catch (Exception $e) {
                        $this->addFlash('danger', 'erreur lors de l\'enregistrement');
                        return $this->redirectToRoute('product');
                    }
                    return $this->redirectToRoute('product_view', ['id' => $product->getId()]);
                }
            }
        } else {
            $this->addFlash('danger', 'Accès refusé !');
            return $this->redirectToRoute('index');
        }

        return $this->render(
            'product/update.html.twig',
            [
                'product' => $product,
                "updateForm" => $form->createView()
            ]
        );
    }

    private function removeProductPicture(string $path)
    {
        $fs = new Filesystem();
        try {
            $fs->remove($this->getParameter('picture_product') . '/' . $path);
        } catch (IOException $e) {
            throw new HttpException(500, 'une erreur s\'est produit lors de la suppression du fichier');
        }
    }

    #[Route('/delete/ {id}', name: 'product_delete', methods: ['POST'])]
    public function delete(Product $product, Request $request, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($product) {
            if ($this->isCsrfTokenValid('delete' . $product->getId(), $request->request->get('token'))) {

                try {
                    $em->remove($product);
                    $em->flush();
                } catch (Exception $e) {
                    $this->addFlash('danger', 'Suppression impossible');
                    return $this->redirectToRoute('product');
                }
                $this->removeProductPicture($product->getPicture());
            }
            return $this->redirectToRoute('product');
        } else {
            return ('Produit inexistant !');
        }
    }   
}
