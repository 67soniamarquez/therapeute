<?php

namespace App\Controller;

use App\Repository\CalendarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainCalendarController extends AbstractController
{
    #[Route('/main/calendar', name: 'main_calendar', methods: ['GET'])]
    public function index(CalendarRepository $calendarRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $events = $calendarRepository->findAll();
        //On récupère les dates en string, pour pouvoir afficher mes rdv, il va falloir d'abord les convertir.
        //on parse directement dans le controller et on les transforme en json à la volée avant de les envoyer à la vue.
    
        //on initialise un tableau vide
        $rdvs = [];
        foreach ($events as $event) {
            //on parse à l'intérieur et on va créer un tableau avec les informations qui sont attendues par FullCalendar.
            //à chaque évènements je vais faire un push
            $rdvs[] = [
                'id' => $event->getId(),
                'start' => $event->getStart()->format('Y-m-d H:i:s'),
                'end' => $event->getEnd()->format('Y-m-d H:i:s'),
                'title' => $event->getTitle(),
                'description' => $event->getDescription(),
                'backgroundColor' => $event->getBackgroundColor(),
                'borderColor' => $event->getBorderColor(),
                'textColor' => $event->getTextColor(),
                'allDay' => $event->getAllDay(),
            ];
        }
        $data = json_encode($rdvs);
        return $this->render('calendar/calendar.html.twig', compact('data'));
    }
}
