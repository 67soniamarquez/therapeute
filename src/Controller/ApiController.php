<?php

namespace App\Controller;

use App\Entity\Calendar;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    #[Route('/api', name: 'api')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    #[Route('/api/edit/{id}', name: 'api_edit', methods: ['PUT'])]
    //le ? m'autorise à passer un id qui potentiellement n'existera pas car la methode PUT doit être capable de créer un évènement qui n'existe pas.
    public function majEvent(?Calendar $calendar, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        //on récupère les données
        $donnees = json_decode($request->getContent());

        //on vérifie qu'on à bien toutes les donnees nécessaire
        if (
            isset($donnees->title) && !empty($donnees->title) &&
            isset($donnees->start) && !empty($donnees->start) &&
            isset($donnees->description) && !empty($donnees->description) &&
            isset($donnees->backgroundColor) && !empty($donnees->backgroundColor) &&
            isset($donnees->borderColor) && !empty($donnees->borderColor) &&
            isset($donnees->textColor) && !empty($donnees->textColor)
        ) {
            //les données sont complètes
            //on initialise un code
            $code = 200; //  OK : La page doit être actualisée avec une nouvelle page mise à jour

            //on vérifie si l'id existe
            if (!$calendar) {  
                //on instancie un rendez-vous
                $calendar = new Calendar;

                //On change le code
                $code = 201; //created
            }
            //On hydrate l'objet avec les données
            $calendar->setTitle($donnees->title);
            $calendar->setDescription($donnees->description);
            $calendar->setStart(new DateTime($donnees->start));

            //on vérifie si on a "true" dans allDay
            if ($donnees->allDay) {
                //la fin est la même que le début
                $calendar->setEnd(new DateTime($donnees->start));
                //sinon on enregistre celle qu'on a reçu
            } else {
                $calendar->setEnd(new DateTime($donnees->end));
            }

            $calendar->setAllDay($donnees->allDay);
            $calendar->setBackgroundColor($donnees->backgroundColor);
            $calendar->setBorderColor($donnees->borderColor);
            $calendar->setTextColor($donnees->textColor);

            $em = $this->getDoctrine()->getManager();
            $em->persist($calendar);
                try {
                    $em->flush();
                } catch (Exception $e) {
                    $this->addFlash('danger', 'erreur lors de l\'enregistrement');
                    return $this->redirectToRoute('index');
                }
            //On retourne le code, non pas une vue mais une réponse de HttpFondation
            return new Response('Ok', $code);
        } else {
            //les données sont imcomplètes
            return new Response('Données incomplètes', 404);
        }
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }
}
