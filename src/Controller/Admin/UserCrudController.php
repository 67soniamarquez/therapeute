<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFilters(Filters $filters): Filters //renvoi à edit
    {
        return $filters
            ->add('roles')
            ->add('last_name');
    }


    public function configureFields(string $pageName): iterable //renvoie à create
    {
        return [

            IdField::new('id')->hideOnForm(), //id est affiché partout sauf dans un formulaire
            TextField::new('first_name', 'last_name'),
            TextField::new('last_name'),
            EmailField::new('email'),
            TextField::new('password')->onlyWhenUpdating(),
            ArrayField::new('roles')->hideOnForm(),
            ImageField::new('picture')->setUploadDir('/public/img')->setBasePath('img/'),
            TelephoneField::new('user_phone'),
            TextField::new('address_number'),
            TextField::new('address_type'),
            TextField::new('address_name'),
            TextField::new('address_zipcode'),
            TextField::new('address_city'),
            DateField::new('dob'),
            //'user_phone',
            //rien ou Field::new('') fonctionne aussi
            //TextEditorField::new('description'),
        ];
    }

    public function createEntity(string $entityFqcn)
    {
        $user = new User();

        return new $entityFqcn();
    }
}
