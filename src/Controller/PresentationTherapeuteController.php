<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PresentationTherapeuteController extends AbstractController
{
    #[Route('/presentation/therapeute', name: 'presentation_therapeute')]
    public function index(): Response
    {
        return $this->render('presentation_therapeute/index.html.twig', [
            'controller_name' => 'PresentationTherapeuteController',
        ]);
    }
}
